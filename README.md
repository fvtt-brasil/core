[![Version](https://img.shields.io/badge/dynamic/json?color=blue&style=flat-square&label=Version&prefix=v&query=version&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Fcore%2F-%2Fraw%2Fmaster%2Fpt-BR%2Fmodule.json)](https://gitlab.com/fvtt-brasil/core) [![Compatible](https://img.shields.io/badge/dynamic/json?color=orange&style=flat-square&label=Compatible&prefix=v&query=compatibleCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffoundryvtt-pt-br%2Fcore%2F-%2Fraw%2Fmaster%2Fpt-BR%2Fmodule.json)](http://foundryvtt.com/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow?style=flat-square)](https://opensource.org/licenses/MIT) [![Discord invite](https://img.shields.io/badge/Chat-on_Discord-blue?logo=discord&style=flat-square&logoColor=white)](https://discord.gg/XNC86FBnQ2) [![pt-BR translation](https://img.shields.io/badge/dynamic/json?color=blue&label=pt-BR&style=flat-square&query=%24.progress.0.data.translationProgress&url=https%3A%2F%2Fbadges.awesome-crowdin.com%2Fstats-200008372-2.json)](https://fvtt.crowdin.com/core)



Portuguese (BR) Core
====================

## Português ([english version](#english "English version"))

Esse módulo adiciona o idioma *Português (Brasil)* como uma opção a ser selecionada nas configurações do Foundry VTT. Selecionar essa opção traduzirá vários aspectos da aplicação, como a interface principal, menus e mensagem de erro. Esse módulo não traduz fichas de personagem, compêndios e demais aspectos relacionados a sistemas específicos, para isso utilize os módulos apropriados de tradução dos sistemas.

Se você quer ajudar nesse projeto, seja com contribuições para o código ou tradução, por favor leia o nosso [guia de contribuições](CONTRIBUTING.md), toda ajuda é bem vinda.


### Instalação

A tradução está disponível na lista de Módulos Complementares para instalar com o nome de `Brazilian Portuguese [Core]`.

#### Instalação por Manifesto

Na opção `Add-On Modules` clique em `Install Module` e coloque o seguinte link no campo `Manifest URL`

`https://gitlab.com/fvtt-brasil/core/-/raw/master/pt-BR/module.json`

#### Instalação Manual (Não recomendado)

Se as opções acima não funcionarem, visite a página com os [pacotes](https://gitlab.com/fvtt-brasil/core/-/packages), clique na versão que você deseja usar, faça o download do arquivo `pt-BR.zip` e extraia o conteúdo na pasta `Data/modules/`.

Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo e depois altere o idioma nas configurações.

<br/>

## English

This module adds the language *Português (Brasil)* as an option to be selected in the Foundry VTT settings. Selecting this option will translate various aspects of the application such as the main interface, menus and error messages. This module does not translate character sheets, compendiums and other aspects related to specific systems, for that use the appropriate systems translation modules.

If you want to help with this project, either with contributions to the code or translation, please read our [contributions guide](CONTRIBUTING.md), any help is welcome.


### Installation

The translation is available in the Add-on Modules list to install under the name `Brazilian Portuguese [Core]`.

#### Installation by Manifest

In the option `Add-On Modules` click on `Install Module` and place the following link in the field `Manifest URL`

`https://gitlab.com/fvtt-brasil/core/-/raw/master/pt-BR/module.json`

#### Manual Installation (Not Recommended)

If the above options do not work, visit the page with the [packages](https://gitlab.com/fvtt-brasil/core/-/packages), click on the version you want to use, download the `pt-BR.zip` file and extract the contents into the `Data/modules/` folder.

Once this is done, activate the module in the settings of the world in which you want it and then change the language in the settings.
